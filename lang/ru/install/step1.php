<?php
$MESS["ZT_INSTALL_NEXT"] = "Далее";
$MESS["ZT_SET_LOGIN"] = "Логин входа в ЛК ЗТ";
$MESS["ZT_LOGIN_ERROR"] = "Укажите логин";
$MESS["ZT_SET_PASSWORD"] = "Пароль входа в ЛК ЗТ";
$MESS["ZT_PASSWORD_ERROR"] = "Укажите пароль";
$MESS["ZT_CLIENT_API_KEY_ERROR"] = "Укажите Client api key";
$MESS["ZT_SET_CLIENT_API_KEY"] = "Client api key";
$MESS["ZT_SET_WIDGET_API_KEY"] = "Widget api key";
$MESS["ZT_WIDJET_API_KEY_ERROR"] = "Укажите Widjet api key";