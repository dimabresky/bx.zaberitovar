<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class bx_zaberitovar extends CModule {

    public $MODULE_ID = "bx.zaberitovar";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "N";

    function __construct() {
        $arModuleVersion = array();
        $path_ = str_replace("\\", "/", __FILE__);
        $path = substr($path_, 0, strlen($path_) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage("ZT_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("ZT_MODULE_DESCRIPTION");
        $this->PARTNER_NAME = Loc::getMessage("ZT_PARTNER_NAME");
        $this->PARTNER_URI = "https://gitlab.com/dimabresky/";

        \Bitrix\Main\Loader::includeModule("sale");
    }

    public function copyFiles() {

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/tools", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tools", true, true);

        return true;
    }

    public function deleteFiles() {

        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/{$this->MODULE_ID}");
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . "/bitrix/tools/{$this->MODULE_ID}");

        return true;
    }

    public function DoInstall() {

        global $APPLICATION;

        try {

            # проверка зависимостей модуля
            if (!ModuleManager::isModuleInstalled("sale")) {
                $errors[] = Loc::getMessage("ZT_CHECK_SALE_INSTALL_ERROR");
            }

            if (isset($errors) && !empty($errors)) {
                throw new Exception(implode("<br>", $errors));
            }

            if (!isset($_REQUEST["zt_install"])) {
                $APPLICATION->IncludeAdminFile(Loc::getMessage("ZT_INSTALL"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/step1.php");
            } else {

                # регистрируем модуль
                ModuleManager::registerModule($this->MODULE_ID);

                # копирование файлов
                $this->copyFiles();

                # создаем службы доставки
                $this->addDelivery();

                # добавление свойств заказа
                $this->addOrderProperties();

                # добавление статусов заказа
                $this->addOrderStatuses();

                # регистрируем зависимости модуля
                $this->addModuleDependencies();

                Option::set($this->MODULE_ID, "ZT_LOGIN", $_REQUEST["zt_install"]["login"]);
                Option::set($this->MODULE_ID, "ZT_PASSWORD", $_REQUEST["zt_install"]["password"]);
                Option::set($this->MODULE_ID, "ZT_CLIENT_API_KEY", $_REQUEST["zt_install"]["client_api_key"]);
                Option::set($this->MODULE_ID, "ZT_WIDGET_API_KEY", $_REQUEST["zt_install"]["widget_api_key"]);
                Option::set($this->MODULE_ID, "ZT_DELIVERY_STATUS_SEND", $_REQUEST["zt_install"]["status"]);
            }
        } catch (Exception $ex) {

            $APPLICATION->ThrowException($ex->getMessage());

            $this->DoUninstall();

            return false;
        }

        return true;
    }

    public function DoUninstall() {
        global $APPLICATION;

        try {

            $arFilter = [
                "DELIVERY_ID" => [
                    Bitrix\Main\Config\Option::get($this->MODULE_ID, "ZT_PVZ_DELIVERY_ID"),
                    Bitrix\Main\Config\Option::get($this->MODULE_ID, "ZT_COURIER_DELIVERY_ID")
                ]
            ];

            $dbOrders = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter)->Fetch();
            
            if ($dbOrders) {
                throw new Exception(Loc::getMessage("ZT_DELIVERY_HAVE_ORDERS"));
            }
            
            # удаление файлов
            $this->deleteFiles();

            # создаем службы доставки
            $this->deleteDelivery();

            # удаление свойств заказа
            $this->deleteOrderProperties();

            # удаление статусов заказа
            $this->deleteOrderStatuses();

            # удаление зависимостей
            $this->deleteModuleDependencies();

            Option::delete($this->MODULE_ID);

            ModuleManager::unRegisterModule($this->MODULE_ID);

            return true;
        } catch (Exception $ex) {

            $APPLICATION->ThrowException($ex->getMessage());

            return false;
        }

        return true;
    }

    public function addDelivery() {

        $fields = [
            "CODE" => "ztpvz",
            "PARENT_ID" => 0,
            "NAME" => Loc::getMessage("ZT_PVZ_DELIVERY_NAME"),
            "ACTIVE" => "Y",
            "VAT_ID" => 0,
            "LOGOTIP" => null,
            "DESCRIPTION" => Loc::getMessage("ZT_PVZ_DELIVERY_DESC"),
            "SORT" => 100,
            "CURRENCY" => Option::get("sale", "default_currency", "RUB"),
            "ALLOW_EDIT_SHIPMENT" => "N",
            "CLASS_NAME" => "\\Bitrix\\Sale\\Delivery\\Services\\Configurable"
        ];

        $pvz_delivery_res = \Bitrix\Sale\Delivery\Services\Manager::add($fields);

        if (!$pvz_delivery_res->getId()) {
            throw new \Exception(Loc::getMessage("ZT_PVZ_DELIVERY_ERROR"));
        }

        Option::set($this->MODULE_ID, "ZT_PVZ_DELIVERY_ID", $pvz_delivery_res->getId());

        $fields["CODE"] = "ztcourier";
        $fields["NAME"] = Loc::getMessage("ZT_COURIER_DELIVERY_NAME");
        $fields["DESCRIPTION"] = Loc::getMessage("ZT_COURIER_DELIVERY_DESC");

        $courier_delivery_res = \Bitrix\Sale\Delivery\Services\Manager::add($fields);

        if (!$courier_delivery_res->getId()) {
            throw new \Exception(Loc::getMessage("ZT_COURIER_DELIVERY_ERROR"));
        }

        Option::set($this->MODULE_ID, "ZT_COURIER_DELIVERY_ID", $courier_delivery_res->getId());

        return true;
    }

    public function deleteDelivery() {

        if (intval(Option::get($this->MODULE_ID, "ZT_PVZ_DELIVERY_ID")) > 0) {
			\Bitrix\Sale\Delivery\Services\Manager::delete(intval(Option::get($this->MODULE_ID, "ZT_PVZ_DELIVERY_ID")));
		}

		if (intval(Option::get($this->MODULE_ID, "ZT_COURIER_DELIVERY_ID")) > 0) {
			\Bitrix\Sale\Delivery\Services\Manager::delete(intval(Option::get($this->MODULE_ID, "ZT_COURIER_DELIVERY_ID")));
		}

        return true;
    }

    public function addOrderProperties() {

        $propsOptions = [];
        $properties = include $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/order_properties.php";
        $result = CSalePersonType::GetList(array('SORT' => 'ASC', 'NAME' => 'ASC'), array());
        while ($row = $result->Fetch()) {
            $propGroup = \CSaleOrderPropsGroup::GetList(($b = "NAME"), ($o = "ASC"), Array('PERSON_TYPE_ID' => $row["ID"]))->Fetch();

            foreach ($properties as $prop_crate_data) {
                $insert = \Bitrix\Sale\Internals\OrderPropsTable::add([
                            "PERSON_TYPE_ID" => $row["ID"],
                            "PROPS_GROUP_ID" => $propGroup["ID"],
                            "CODE" => $prop_crate_data["CODE"],
                            "NAME" => $prop_crate_data["NAME"],
                            "ACTIVE" => "Y",
                            "TYPE" => "STRING",
                            "UTIL" => $prop_crate_data["UTIL"],
                            "SORT" => 100,
                            "ENTITY_REGISTRY_TYPE" => "ORDER"
                ]);
                if ($insert->isSuccess()) {
                    $propsOptions[$row["ID"]][$insert->getId()] = $prop_crate_data["CODE"];

                    CSaleOrderProps::UpdateOrderPropsRelations($insert->getId(), [Option::get($this->MODULE_ID, "ZT_PVZ_DELIVERY_ID"), Option::get($this->MODULE_ID, "ZT_COURIER_DELIVERY_ID")], "D");
                }
            }
        }

        if (empty($propsOptions)) {
            throw new Exception(Loc::getMessage("ZT_ADD_PROPERTIES_ERROR"));
        }

        Option::set($this->MODULE_ID, "ZT_ORDER_PROPERTIES", serialize($propsOptions));
    }

    public function deleteOrderProperties() {

        $propsOptions = unserialize(Option::get($this->MODULE_ID, "ZT_ORDER_PROPERTIES"));

        if (!empty($propsOptions) && is_array($propsOptions)) {
            foreach ($propsOptions as $arr) {
                $propsIds = \array_keys($arr);
                foreach ($propsIds as $pid) {
                    CSaleOrderProps::Delete($pid);
                }
            }
        }
    }

    public function addOrderStatuses() {

        $statuses = include $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/{$this->MODULE_ID}/install/order_statuses.php";
        $statusesOptions = $langsData = [];

        $dbLangs = CLangAdmin::GetList(($b = "sort"), ($o = "asc"), array("ACTIVE" => "Y"));

        while ($arLang = $dbLangs->Fetch()) {
            $langsData[] = ["LID" => $arLang["ID"], "NAME" => "", "DESCRIPTION" => ""];
        }

        $sort = 1000;
        $sale_administrator_group = \Bitrix\Main\GroupTable::getList([
                    "filter" => ["STRING_ID" => "sale_administrator"],
                    "select" => ["ID"]
                ])->fetch();

        $sale_task = $result = Bitrix\Main\TaskTable::getList(array(
                    'select' => array('ID'),
                    'filter' => array('=MODULE_ID' => 'sale', '=BINDING' => 'status', "LETTER" => "X"),
                ))->fetch();


        foreach ($statuses as $status) {

            if (Bitrix\Sale\Internals\StatusTable::add([
                        "ID" => $status["BX_CODE"],
                        "SORT" => $sort,
                        "NOTIFY" => "Y",
                        "TYPE" => "O"
                    ])) {

                foreach ($langsData as $langData) {
                    Bitrix\Sale\Internals\StatusLangTable::add(\array_merge($langData, ["NAME" => $status["NAME"], "STATUS_ID" => $status["BX_CODE"]]));
                }

                if (!empty($sale_administrator_group)) {
                    Bitrix\Sale\Internals\StatusGroupTaskTable::add([
                        "STATUS_ID" => $status["BX_CODE"],
                        "GROUP_ID" => intval($sale_administrator_group["ID"]),
                        "TASK_ID" => intval($sale_task["ID"])
                    ]);
                }

                $sort += 100;

                $statusesOptions[$status["XML_ID"]] = $status["BX_CODE"];
            }
        }

        if (empty($statusesOptions)) {
            throw new Exception(Loc::getMessage("ZT_ADD_STATUSES_ERROR"));
        }

        Option::set($this->MODULE_ID, "ZT_ORDER_STATUSES", serialize($statusesOptions));
    }

    public function deleteOrderStatuses() {
        $statusesOptions = unserialize(Option::get($this->MODULE_ID, "ZT_ORDER_STATUSES"));

        if (!empty($statusesOptions) && is_array($statusesOptions)) {
            foreach ($statusesOptions as $statusId) {

                Bitrix\Sale\Internals\StatusGroupTaskTable::deleteByStatus($statusId);
                Bitrix\Sale\Internals\StatusLangTable::deleteByStatus($statusId);
                CSaleStatus::Delete($statusId);
            }
        }
    }

    public function addModuleDependencies() {

        RegisterModuleDependences("main", "OnAdminContextMenuShow", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onAdminContextMenuShow");
        RegisterModuleDependences("sale", "onSaleDeliveryServiceCalculate", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleDeliveryServiceCalculate");
        RegisterModuleDependences("sale", "OnSaleComponentOrderProperties", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleComponentOrderProperties");
        RegisterModuleDependences("sale", "OnSaleComponentOrderJsData", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleComponentOrderJsData");
        RegisterModuleDependences("sale", "OnSaleOrderBeforeSaved", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleOrderBeforeSaved");
        RegisterModuleDependences("sale", "OnSaleStatusOrderChange", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleStatusOrderChange");
        RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleComponentOrderOneStepDelivery");
    }

    public function deleteModuleDependencies() {

        UnRegisterModuleDependences("main", "OnAdminContextMenuShow", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onAdminContextMenuShow");
        UnRegisterModuleDependences("sale", "onSaleDeliveryServiceCalculate", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleDeliveryServiceCalculate");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderProperties", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleComponentOrderProperties");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderJsData", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleComponentOrderJsData");
        UnRegisterModuleDependences("sale", "OnSaleOrderBeforeSaved", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleOrderBeforeSaved");
        UnRegisterModuleDependences("sale", "OnSaleStatusOrderChange", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleStatusOrderChange");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "\\zaberitovar\\EventsHandlers", "onSaleComponentOrderOneStepDelivery");
    }

}
