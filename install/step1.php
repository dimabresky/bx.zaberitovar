<?php
global $APPLICATION;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

    ?>
<form action="<? echo $APPLICATION->GetCurPage() ?>" name="form1" onsubmit="return validateZTInstallForm(this)">
    <?= bitrix_sessid_post() ?>

        <input type="hidden" name="lang" value="<? echo LANG ?>">
        <input type="hidden" name="id" value="bx.zaberitovar">
        <input type="hidden" name="install" value="Y">
        <table cellpadding="3" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td align="right" width="40%"><b><?= Loc::getMessage("ZT_SET_LOGIN") ?></b></td>
                    <td align="left" width="60%">
                        <input name="zt_install[login]" type="text">
                    </td>
                </tr>
                <tr>
                    <td align="right" width="40%"><b><?= Loc::getMessage("ZT_SET_PASSWORD") ?></b></td>
                    <td align="left" width="60%">
                        <input name="zt_install[password]" type="text">
                    </td>
                </tr>
                <tr>
                    <td align="right" width="40%"><b><?= Loc::getMessage("ZT_SET_CLIENT_API_KEY") ?></b></td>
                    <td align="left" width="60%">
                        <input name="zt_install[client_api_key]" type="text">
                    </td>
                </tr>
                <tr>
                    <td align="right" width="40%"><b><?= Loc::getMessage("ZT_SET_WIDGET_API_KEY") ?></b></td>
                    <td align="left" width="60%">
                        <input name="zt_install[widget_api_key]" type="text">
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2" align="right"><input class="adm-btn-save" type="submit" name="inst" value="<? echo Loc::getMessage("ZT_INSTALL_NEXT") ?>"></td>
                </tr>
            </tfoot>
        </table>

    </form>

<script>
function validateZTInstallForm() {
    
    var login = document.querySelector("input[name=\"zt_install[login]\"]");
    var password = document.querySelector("input[name=\"zt_install[password]\"]");
    var client_api_key = document.querySelector("input[name=\"zt_install[client_api_key]\"]");
    var widget_api_key = document.querySelector("input[name=\"zt_install[widget_api_key]\"]");
    var errors = [];
    
    if (login.value === "") {
        errors.push("<?= Loc::getMessage("ZT_LOGIN_ERROR")?>");
    }
    if (password.value === "") {
        errors.push("<?= Loc::getMessage("ZT_PASSWORD_ERROR")?>");
    }
    if (client_api_key.value === "") {
        errors.push("<?= Loc::getMessage("ZT_CLIENT_API_KEY_ERROR")?>");
    }
    if (widget_api_key.value === "") {
        errors.push("<?= Loc::getMessage("ZT_WIDGET_API_KEY_ERROR")?>");
    }
    
    if (errors.length) {
        alert(errors.join("\n"));
        return false;
    }
    
    return true;
}</script>