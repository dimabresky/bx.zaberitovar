<?php

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<div class="zt-courier-delivery-description-block">
    <input name="zt_courier[cod]" value="<?= \htmlspecialcharsbx($arData["cod"]) ?>" type="hidden">
    <input name="zt_courier[cityname]" value="<?= \htmlspecialcharsbx($arData["cityname"]) ?>" type="hidden">
    <input name="zt_courier[srok]" value="<?= \htmlspecialcharsbx($arData["srok"]) ?>" type="hidden">
    <input name="zt_courier[partner]" value="<?= \htmlspecialcharsbx($arData["partner"]) ?>" type="hidden">
    <input name="zt_courier[price]" value="<?= \htmlspecialcharsbx($arData["price"]) ?>" type="hidden">
    <?php
    $title = Loc::getMessage("ZT_COURIER_OPEN_WIDGET_SET_POINT_TITLE");
    if (!empty($arData["cod"])):
        $title = Loc::getMessage("ZT_COURIER_OPEN_WIDGET_CHANGE_POINT_TITLE");
        echo Loc::getMessage("ZT_COURIER_ADDRESS", ["#ADDRESS#" => $arData["desc"]]);
        ?>
        <br>
        <br>
    <? endif ?>
    <a href="javascript:void(0)" onclick="BX.zaberitovar.courierOpenWidget('<?= \htmlspecialcharsbx($arData["widget_api_key"]) ?>')"><?= $title ?></a>

</div>
